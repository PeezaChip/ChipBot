using ChipBot;
using ChipBot.Core.Abstract.Interfaces;
using ChipBot.Core.Extensions;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<IBot>(new Bot());
builder.AddChipCore([
    Assembly.GetExecutingAssembly(),
#if DEBUG
    Assembly.GetAssembly(typeof(DebugPlugin.Plugin)),
#endif
    Assembly.GetAssembly(typeof(PointsPlugin.Plugin)),
    Assembly.GetAssembly(typeof(RafflePlugin.Plugin)),
    Assembly.GetAssembly(typeof(BetPlugin.Plugin)),
    Assembly.GetAssembly(typeof(WrongPlugin.Plugin)),
    Assembly.GetAssembly(typeof(BirthdayPlugin.Plugin)),
    Assembly.GetAssembly(typeof(AnimalPlugin.Plugin)),
    Assembly.GetAssembly(typeof(CurrencyPlugin.Plugin)),
    Assembly.GetAssembly(typeof(RewriterPlugin.Plugin)),
    Assembly.GetAssembly(typeof(KanobuPlugin.Plugin)),
    Assembly.GetAssembly(typeof(NineGagPlugin.Plugin)),
    Assembly.GetAssembly(typeof(WednesdayPlugin.Plugin)),
    Assembly.GetAssembly(typeof(HoyolabPlugin.Plugin)),
    Assembly.GetAssembly(typeof(RSSPlugin.Plugin)),
    Assembly.GetAssembly(typeof(PoEPlugin.Plugin)),
    Assembly.GetAssembly(typeof(WoWPlugin.Plugin)),
    Assembly.GetAssembly(typeof(AttendancePlugin.Plugin)),
    Assembly.GetAssembly(typeof(SocialRatingPlugin.Plugin)),
]);

var app = builder.Build();

app.UseChipCore();

app.Run();
