﻿using ChipBot.Core.Implementations;
using Discord;
using Discord.Interactions;

namespace ChipBot.Modules
{
    [RequireContext(ContextType.Guild)]
    [Group("moderator", "moderator commands")]
    public class ModeratorModule : ChipInteractionModuleBase
    {
        [SlashCommand("clean", "Cleans last 100 (by default) messages in channel")]
        [RequireUserPermission(ChannelPermission.ManageMessages)]
        public async Task Clean([Summary("number", "Number of messages to clean")] int n = 100)
        {
            if (n > 200)
            {
                await ReplyError("Cant't remove more than 200 messages at once");
                return;
            }
            var msgs = await Context.Channel.GetMessagesAsync(n).FlattenAsync();
            await ((ITextChannel)Context.Channel).DeleteMessagesAsync(msgs);
            await ReplySuccessAsync(true);
        }
    }
}
