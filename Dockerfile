FROM mcr.microsoft.com/dotnet/core/runtime:3.1

# Set up project dir
RUN mkdir /usr/local/chipbot
COPY ./src/Build/netcoreapp3.1/publish/ /usr/local/chipbot/publish

WORKDIR /usr/local/chipbot

ENTRYPOINT ["dotnet", "publish/ChipBot.dll"]