## v4.3.0 - 19.03.2023
### Changed
* Updated victoria to v6

## v4.2.1 - 18.03.2023
### Changed
* Rewrite plugin update

## v4.2.0 - 17.03.2023
### Added
* Riot plugin

## v4.1.0 - 15.03.2023
### Added
* OpenAI plugin
* Hoyolab Key parsers
* Hoyolab daily notes & abyss

## v4.0.0 - 21.10.2022
### Added
* Interactions support

## v3.0.4 - 17.10.2022
### Added
* Hoyolab plugin

## v3.0.3 - 05.09.2022
### Added
* Cat plugin

## v3.0.2 - 29.08.2022
### Added
* Currency plugin

## v3.0.1 - 02.12.2021
### Fixed
* Fixed an issue when bot wouldn't leave voice channel
* Fixed an issue when vote command would stop responding
* Fixed an issue where 9gag wednesday plugin would post same picture

## v3.0.0 - 03.11.2021
### Added
* Yes

## v2.2.7.0 - 10.11.2020
### Added
* !vote command

## v2.2.6.2 - 21.10.2020
### Changed
* Updated core version

## v2.2.6.1 - 24.08.2020
### Fixed
- LavaService minor fixes
- Service start timeout

## v2.2.6.0 - 05.08.2020
### Added
* IonotekaPlugin

## v2.2.5.2 - 17.07.2020
### Added
* gift limiter
### Changed
* paged leaderboard

## v2.2.5.1 - 15.07.2020
### Changed
* TextToImage plugin now uses monospace font

## v2.2.5.0 - 14.07.2020
### Added
* TextToImage plugin

## v2.2.4.0 - 12.07.2020
### Added
* Interactive update
* Betting plugin

## v2.2.3.1 - 17.06.2020
### Changed
* updated core version

## v2.2.3.0 - 17.06.2020
### Added
* WednesdayPlugin which will post a meme video on a wednesday

## v2.2.2.4 - 16.06.2020
### Fixed
* NinePlugin plugin regex groups count fix

## v2.2.2.3 - 05.06.2020
### Added
* Leonardo plugin

## v2.2.2.2 - 22.05.2020
### Fixed
* NinePlugin return webm to regex

## v2.2.2.1 - 19.05.2020
### Fixed
* NinePlugin regex fix

## v2.2.2.0 - 14.05.2020
### Added
* Kanobu plugin

## v2.2.1.0 - 13.05.2020
### Changed
* updated core version
* updated plugins to support INotifySupport