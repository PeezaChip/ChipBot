using ChipBot.Client;
using ChipBot.Client.Services;
using ChipBot.Client.Shared;
using ChipBot.Core.Web.Extensions;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using System.Reflection;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

builder.Services.AddSingleton<ThemeManager>();

builder.Services.AddChipBotWebCore(new List<Assembly>() {
    Assembly.GetExecutingAssembly(),
    Assembly.GetAssembly(typeof(PointsPlugin.Web.WebPlugin)),
    Assembly.GetAssembly(typeof(RafflePlugin.Web.WebPlugin)),
    Assembly.GetAssembly(typeof(WrongPlugin.Web.WebPlugin)),
    Assembly.GetAssembly(typeof(BirthdayPlugin.Web.WebPlugin)),
    Assembly.GetAssembly(typeof(AnimalPlugin.Web.WebPlugin)),
    Assembly.GetAssembly(typeof(CurrencyPlugin.Web.WebPlugin)),
    Assembly.GetAssembly(typeof(HoyolabPlugin.Web.WebPlugin)),
    Assembly.GetAssembly(typeof(WoWPlugin.Web.WebPlugin)),
    Assembly.GetAssembly(typeof(AttendancePlugin.Web.WebPlugin)),
}, typeof(AppLayout));

await builder.Build().RunAsync();
